import logging

from asyncio import AbstractEventLoop, Protocol, Transport

log = logging.getLogger(__name__)


class Client(Protocol):
    def __init__(self, loop: AbstractEventLoop):
        super().__init__()
        self.loop = loop
        self.transport = None
        self.address = None
        self.data = b""
        self.head = b""
        self.body = b""

    def connection_made(self, transport: Transport) -> None:
        """
        Triggered when a client connects to a server, also
        prompts the user to input text to be sent to the server

        :param transport: the socket to the server
        """

        self.transport = transport
        self.address = transport.get_extra_info("peername")
        log.info("Connected to ({}, {})".format(*self.address))
        self.start_menu()

    def start_menu(self) -> None:
        """
        A 'menu' to prompt the user for input to be sent
        """

        msg = input("> ").encode("utf-8")
        self.transport.write(msg)
        log.debug(f"Sending... {msg}")

    def data_received(self, data: bytes) -> None:
        """
        Triggered when the client receives bytes from the server

        :param data: bytes sent from server
        """

        log.debug(data)
        if b".\r\n" in data:
            # End of data
            return self.process_response()
        self.data += data

    def process_response(self):
        self.head, *self.body = self.data.decode().split("\n", maxsplit=1)
        self.body = "".join(self.body)
        self.data = b""

        log.info(f"Head {self.head}")
        log.info(f"Body {self.body}")
        self.start_menu()

    def eof_received(self) -> None:
        """
        If the client receives an EOF, close the socket
        to the server, which triggers connection_lost
        and closes the event loop.
        """

        log.debug("received EOF")
        self.transport.close()

    def connection_lost(self, exc) -> None:
        """
        Triggered when the client loses connection to it's server,
        the client closes the loop it's running on then.
        """
        log.info("server closed connection")
        self.loop.stop()
