import logging

from .client import Client
from asyncio import get_event_loop


HOST = "127.0.0.1"
PORT = 1337
log = logging.getLogger(__name__)

if __name__ == "__main__":
    loop = get_event_loop()
    try:
        while True:
            coro = loop.create_connection(
                lambda: Client(loop), HOST, PORT
            )
            log.debug("Starting client")
            loop.run_until_complete(coro)
            loop.run_forever()
    finally:
        loop.close()
