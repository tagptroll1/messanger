import logging
import sys
from logging import StreamHandler

logging.basicConfig(
    format="%(asctime)s Secure Messenger: | %(name)15s | %(levelname)6s | %(message)s",
    datefmt="%b %d %H:%M:%S",
    level=logging.DEBUG,
    handlers=[StreamHandler(stream=sys.stdout)]
)

log = logging.getLogger(__name__)

for key, value in logging.Logger.manager.loggerDict.items():
    if not isinstance(value, logging.Logger):
        continue

    value.setLevel(logging.DEBUG)

    for handler in value.handlers.copy():
        value.removeHandler(handler)


logging.getLogger("socket").setLevel(logging.ERROR)
