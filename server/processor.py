

class Processor:

    @staticmethod
    def ECHO(byte: bytes) -> bytes:
        return byte

    @staticmethod
    def REGISTER(db, byte: bytes) -> bytes:
        return b"200 OK"

    @staticmethod
    def handle_register(db, byte: bytes) -> bool:
        pass
