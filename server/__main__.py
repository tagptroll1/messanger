import logging
from .server import ServerProtocol
from .database import UserBase

from asyncio import get_event_loop
from functools import partial

HOST = "127.0.0.1"
PORT = 1337

log = logging.getLogger(__name__)

if __name__ == "__main__":
    loop = get_event_loop()
    server = partial(ServerProtocol, userdb=UserBase)
    factory = loop.create_server(server, HOST, PORT)
    server = loop.run_until_complete(factory)
    log.debug("Starting up server")

    try:
        loop.run_forever()
    finally:
        log.debug("Closing server")
        server.close()
        loop.run_until_complete(server.wait_closed())
        log.debug("Closing event loop")
        loop.close()
