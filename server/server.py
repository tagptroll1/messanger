import logging

from .processor import Processor
from asyncio import Protocol, Transport

log = logging.getLogger(__name__)

ECHO = b"ECHO"
REGISTER = b"REGISTER"
SPACE = b" "
CRLF = b".\r\n"
LINE = b"\n"
status = {
    200: b"200 OK",
    404: b"404 Not found",
    405: b"405 Method Not Allowed",
}


class Response:
    def __init__(self, head: bytes):
        self.head = head + b"\n"
        self.body = b""

    def feed(self, data):
        self.body += data

    def more(self):
        if self.head:
            temp = self.head
            self.head = ""
            return temp

        elif self.body:
            if len(self.body) > 512:
                chunk = self.body[:512]
                self.body = self.body[512:]
                return chunk
            else:
                temp = self.body
                self.body = b""
                return temp
        return CRLF


class ServerProtocol(Protocol):
    def __init__(self, userdb):
        self.transport = None
        self.userdb = userdb
        self.msgdb = None

    def connection_made(self, transport: Transport) -> None:
        """
        Triggered when a client connects to the server

        :param transport: the socket to the client
        """

        peer_name = transport.get_extra_info('peername')
        log.debug(f"Connection made {peer_name}")
        self.transport = transport

    def data_received(self, data: bytes) -> None:
        """
        Triggered when the server receives bytes from the client

        :param data: bytes sent from client
        """

        log.info(f"Received {data}")
        self.process_request(data)

    def process_request(self, data: bytes) -> None:
        """
        Used to process the bytes sent, split into request and text.
        Handles the text based on the request, responds with
        405 Method not found if the request is not available.

        Available requests;
        ECHO: returns the bytes sent
        :param data: bytes received from data_received
        """

        request, *text = data.split()
        text = SPACE.join(text)

        log.debug(text)

        if request.upper() == ECHO:
            self.send(status[200], Processor.ECHO(text))
        elif request.upper() == REGISTER:
            self.send(Processor.REGISTER(self.userdb, text))
        else:
            log.info("Got unknown request, sending 405")
            body = b"%b is an unknown request\n" % request
            self.send(status[405], body)

    def send(self, head: bytes, body: bytes = b"") -> None:
        """
        Sends bytes back to the client through the
        stored transport in the form of a status message
        "200 OK" followed by the response.

        :param head: bytes to be sent as header
        :param body: bytes to be sent as body
        """

        resp = Response(head)
        resp.feed(body)

        log.info(f"Sending... response - length: {len(resp.body)}")
        chunk = b""
        while chunk != CRLF:
            chunk = resp.more()
            log.debug(chunk)
            self.transport.write(chunk)

    def eof_received(self) -> None:
        """
        If the server receives an EOF, respond to the client
        with an EOF, if possible to close the client session.
        """

        log.debug('received EOF')
        if self.transport.can_write_eof():
            self.transport.write_eof()
