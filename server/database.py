import sqlite3 as sqlite


class Database:
    __slots__ = ("conn",)

    def __init__(self, path):
        self.conn = sqlite.connect(path)
        self.create_table()

    def create_table(self):
        user_table = (
            """
            CREATE TABLE IF NOT EXISTS users(
              id int PRIMARY KEY,
              username text UNIQUE,
              password text NOT NULL 
            );
            """
        )

        message_table = (
            """
            CREATE TABLE IF NOT EXISTS messages(
              id int PRIMARY KEY,
              authorid INTEGER NOT NULL,
              content text NOT NULL,
              posted timestamp DEFAULT current_timestamp,
              FOREIGN KEY (authorid) REFERENCES users(id)
            );
            """
        )

        with self.conn as conn:
            conn.execute(user_table)
            conn.execute(message_table)


class UserBase(Database):
    __slots__ = "adduser",

    def __init__(self, path):
        super.__init__(path)
        self.adduser = self.add_user

    def add_user(self, username, password):
        query = (
            """
            INSERT INTO OR FAIL users(id, username, password)
              VALUES (?, ?, ?);
            """
        )
        try:
            with self.conn as conn:
                conn.execute(query, (1, username, password))
        except Exception as e:
            print(e)
            return False
        return True
